# SOMMAIRE

## Déploiment automatisé

  - [Terraform](https://gitlab.com/cyrille_/terraform)
  - [Ansible Deploiement](https://gitlab.com/cyrille_/ansible-deploiement)
  - [Vagrant docker-compose](https://gitlab.com/cyrille_/vagrant-docker-compose)
  - [Wordpress docker-compose](https://gitlab.com/cyrille_/wordpress-docker-compose)
  - [Ansible Vagrant](https://gitlab.com/cyrille_/ansible-vagrant)
  - [Packer](https://gitlab.com/cyrille_/packer)
  - [Projet Java](https://gitlab.com/cyrille_/mon-projet-java)

## Intégration continue
 
  - [Gitlab-ci Harbor Sonarqube + traefik](https://gitlab.com/cyrille_/smart-contenanair)
  - [Gitlab-ci Prometheus Grafana + traefik](https://gitlab.com/cyrille_/tesla-container)

## Servers

  - [Wordpress docker-compose](https://gitlab.com/cyrille_/wordpress-docker-compose)
  - [Caddy docker-compose](https://gitlab.com/cyrille_/caddy-http-local)
  - [Lamp docker-compose](https://gitlab.com/cyrille_/lamp)  
  - [Nextcloud docker-compose](https://gitlab.com/cyrille_/nexcloud-traefik-me-docker-compose)

## Scripting Bash

  - [Quelques scripts Bash](https://gitlab.com/cyrille_/scripts-bash)

## Scripting Python

  - [Quelques scripts Python](https://gitlab.com/cyrille_/scripts-python)

## Projets

  - [Projet Authelia 1 : Déploiement infra en 1 étape]
  - [Projet Authelia 2 : Déploiement infra en 2 étapes]
  - [Projet Authelia 3 : Déploiement infra en 3 étapes](https://gitlab.com/cyrille_/projet-authelia-sommaire)
